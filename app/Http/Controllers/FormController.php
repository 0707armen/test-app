<?php

namespace App\Http\Controllers;

use App\Enums\SaveFormTypeEnum;
use App\Forms\Factories\SaveFormFactory;
use App\Http\Requests\SaveFormRequest;
use App\Struct\FormStruct;
use App\Traits\RespondsWithHttpStatus;
use Exception;

class FormController extends Controller
{
    use RespondsWithHttpStatus;

    public function create(SaveFormRequest $request)
    {
        $data = $request->validationData();
        $form = new FormStruct();
        $form->name = $data['name'];
        $form->phone = $data['phone'];
        $form->message = $data['message'];
        try {
            $createForm = SaveFormFactory::getInstance(SaveFormTypeEnum::SAVE_FORM_TO_FILE, $form);
            $createForm->saveForm();
            return $this->success('Form successfully saved');
        }catch (Exception $exception){
            return $this->failure('please try again later', 500);
        }

    }
}
