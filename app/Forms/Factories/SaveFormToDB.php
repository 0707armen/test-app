<?php

namespace App\Forms\Factories;

use App\Struct\FormStruct;
use Illuminate\Support\Facades\DB;

class SaveFormToDB implements SaveFormFactoryInterface
{
    private $form;
    private $database;

    public function __construct(FormStruct $form)
    {
        $this->form = $form;
        $this->database = DB::connection('saveForm');

    }
    public function saveForm()
    {
        try {
            $this->database->table('forms')->insert([
                'name' => $this->form->name,
                'phone' => $this->form->phone,
                'message' => $this->form->message,
            ]);
        } catch(Exception $e) {
        }

    }

}
