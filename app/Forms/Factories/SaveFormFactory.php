<?php

namespace App\Forms\Factories;

use App\Struct\FormStruct;
use Exception;

class SaveFormFactory
{
    /**
     * @param string $className
     * @param FormStruct $form
     * @return mixed
     * @throws Exception
     */
    public static function getInstance($className, FormStruct $form)
    {
        $class = __NAMESPACE__."\\".$className;
        if (class_exists($class)) {
            return new $class($form);
        }
        else {
            throw new Exception("Invalid class name.");
        }
    }
}
