<?php


namespace App\Forms\Factories;


use App\Struct\FormStruct;
use Illuminate\Support\Facades\Storage;

class SaveFormToFile implements SaveFormFactoryInterface
{
    private $form;

    public function __construct(FormStruct $form)
    {
        $this->form = $form;
    }

    public function saveForm()
    {
        try {
            $name = isset($this->form->name) ? $this->form->name : '-';
            $phone = isset($this->form->phone) ? $this->form->phone : '-';
            $message = isset($this->form->message) ? $this->form->message : '-';
            $output = "---------------------------------" . "\n";
            $output .= date("d-m-Y H:i:s") . "\n";
            $output .= "Name: " . $name . "\n";
            $output .= "Phone: " . $phone . "\n";
            $output .= "Message: " . $message . "\n";
            Storage::disk('local')->put('form.txt', $output);

        } catch (Exception $e) {
            throw $e;
        }
    }
}
