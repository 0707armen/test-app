<?php


namespace App\Forms\Factories;


interface SaveFormFactoryInterface
{
    public function saveForm();

}
