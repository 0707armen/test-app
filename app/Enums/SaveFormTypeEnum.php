<?php
namespace App\Enums;

/**
 * Class AdminHourLogTypeEnum
 *
 * @category Enum
 * @package  App\Enums
 * @author   Gopal Joshi <gopal.joshi@brainvire.com>
 * @license  https://insomniacookies.com N/A
 * @link     https://insomniacookies.com
 */

class SaveFormTypeEnum
{
     const SAVE_FORM_TO_FILE = 'SaveFormToFile';
     const SAVE_FORM_TO_DB = 'SaveFormToDB';
}
