import Form from './components/Form.vue';

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);
export default new VueRouter({
    routes: [
            {
                name: 'form',
                path: '/',
                component: Form
            },

    ],
});
